<?php

require_once('./logyak.php');

logyak('should not land!');

define('LOGYAK_ENABLE', true);
define('LOGYAK_ADDR', '127.0.0.1');
define('LOGYAK_PORT', 11111);

logyak('simple message should land');

logyak('containing array', array('asdf', 'fdsa'));
logyak('containing object', array('asdf' => 'fdsa', 'deffefe' => '2134'));

?>