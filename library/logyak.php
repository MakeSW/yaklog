<?php
class LogyakLogger {
    private static $instance;

    private $socket;

    private function __construct() {
    }

    function serializeArgs ( ...$args ) {
        $messages = [];

        foreach ( $args as $arg ) {
            if ( is_string( $arg ) || is_bool( $arg ) || is_numeric( $arg ) ) {
                $messages[] = $arg;
            } else {
                $messages[] = $arg;
            }
        }

        $payload = array(
            'timestamp' => (new DateTime())->getTimestamp(),
            'messages' => $messages
        );

        return json_encode($payload, JSON_PRETTY_PRINT);
    }

    public function log( ...$args ) {
        $this->initSocket();
        $payload = $this->serializeArgs( ...$args );
        $num_of_bytes = strlen( $payload );

        socket_send(
            $this->socket,
            $payload,
            $num_of_bytes,
            MSG_EOF
        );
        socket_close($this->socket);
        $this->socket = null;
    }

    private function initSocket() {
        if ($this->socket) {
            return;
        }

        $this->socket = socket_create(
            AF_INET,
            SOCK_STREAM,
            SOL_TCP
        );
        socket_connect($this->socket, LOGYAK_ADDR, LOGYAK_PORT);
    }

    public static function getInstance() {
        if (!isset(LogyakLogger::$instance)) {
            LogyakLogger::$instance = new LogyakLogger();            
        }

        return LogyakLogger::$instance;
    }
}


function logyak( ...$args ) {
    if ( !defined('LOGYAK_ENABLE') ) {
        return;
    }

    $logger = LogyakLogger::getInstance();
    $logger->log( ...$args );
}

?>
