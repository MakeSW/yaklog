const net = require('net');

const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');

function pad(num) {
    return num > 9 ? `${num}` : `0${num}`;
}

function formatDate(date) {
    const year = pad(date.getFullYear());
    const month = pad(date.getMonth() + 1);
    const day = pad(date.getDate());
    const hours = pad(date.getHours());
    const minutes = pad(date.getMinutes());
    const sec = pad(date.getSeconds());

    return `[${year}-${month}-${day} ${hours}:${minutes}:${sec}]`;
}

function logLine(timestamp, line) {
    const date = new Date(timestamp * 1000);
    const dateToken = formatDate(date)
    console.log(`${dateToken}\t${line}`);
}

const argv = yargs(hideBin(process.argv))
    .option('port', {
        type: 'number',
        describe: 'port number to listen on',
        default: 11111,
    })
    .option('host', {
        type: 'string',
        describe: 'host name or IP to listen on',
        default: '127.0.0.1',
    })
    .help()
    .argv;

console.log("");
console.log("             _.-````'-,_");
console.log("         ,-'`           `'-.,_");
console.log(" /)     (\                   '``-.");
console.log("( ( .,-') )                      ``");
console.log(" \ '   (_/                        !!");
console.log("  |       /)           '          !!!");
console.log("  `\    ^'            '     !    !!!!");
console.log("    !      _/! , !   !  ! !  !   !!!");
console.log("     \Y,   |!!!  !  ! !!  !! !!!!!!!");
console.log("       `!!! !!!! !!  )!!!!!!!!!!!!!");
console.log("        !!  ! ! \( \(  !!!|/!  |/!");
console.log("               /_(/_(    /_(  /_(");
console.log("");
console.log(`Started yaklog server - waiting for connections on ${argv.host}:${argv.port}...`);

net.createServer(sock => {
    sock.on('error', err => {
        throw err;
    });

    sock.on('data', data => {
        const payloadToken = data.toString();

        try {
            const payload = JSON.parse(payloadToken);
            const messages = payload.messages || [];
            const timestamp = payload.timestamp;

            messages.forEach(message => {
                if (typeof message === 'string' || typeof message === 'number' || typeof message === 'boolean') {
                    logLine(timestamp, message);
                } else {
                    logLine(timestamp, '');
                    console.log(JSON.stringify(message, null, 4));
                }
            });
        } catch (e) {
            console.log(e);
            // omitting message
        }
    });
}).listen(argv.port, argv.host);
